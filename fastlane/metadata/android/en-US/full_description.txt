This App alerts you when the battery exceeds the minimum/maximum limit
previously set.

You can select different types of warnings:
  - By Google voice (tells you the percentage).
  - By Google low voice.
  - By a selected tone.
  - By a notification. It depends on the Android version,
    we can add sound (by default silent).
  - By the flash light.

We can configure whether to warn when it is below the minimum and charging.

We can configure whether to warn when it is not charging and pass the maximum.

We can set the time in minutes to check the battery.

Note: On my Huawei P30 lite if the App is in the background it is closed
      by the Huawei itself after a while, so you have to leave it in the foreground to
      in the foreground to warn if the minimum battery has been reached.
      battery has been reached.