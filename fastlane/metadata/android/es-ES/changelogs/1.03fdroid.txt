Añadida metadata structure para F-Droid
Añadido distributionSha256Sum a gradle-wrapper.properties.
Quitados permisos que no se usan en AndroidManifest.xml.
Corregido error en el if que comprueba si hay que notifique aviso.
Corregido para que se desactive Google voz si se activa Voz baja y viceversa.
Corregidas las definiciones public que no son necesarias a private.