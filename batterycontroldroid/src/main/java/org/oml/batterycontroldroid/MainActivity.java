package org.oml.batterycontroldroid;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import java.io.IOException;
import java.util.Locale;
import static java.lang.System.currentTimeMillis;

public class MainActivity extends AppCompatActivity implements
        TextToSpeech.OnInitListener, AdapterView.OnItemSelectedListener, OnClickListener {
    private TextView tvPercentage;
    private Context context;
    private TextToSpeech tts = null;
    private int level = 0;
    private final String myPrefs = "preferences";
    private EditText etMinCharge;
    private EditText etMaxCharge;
    private SwitchCompat swGoogleVoice;
    private SwitchCompat swLowVoice;
    private SwitchCompat swCheckMinCharging;
    private SwitchCompat swCheckMaxNotCharging;
    private MediaPlayer myPlayer = null;
    private String sRingToneUri;
    private final int requestCodeTone = 2758913;
    private EditText etMinutes;
    private int iMinutes;
    private NotificationManagerCompat notificationManagerCompat;
    private NotificationManager notificationManager;
    private Notification.Builder builder;
    private NotificationCompat.Builder builderCompat;
    private long unixTimeStart;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    private NotificationChannel channel;
    private long unixTimeLastCheckBattery;
    private LinearLayout ll_activity_main;
    private final int REQUEST_PHONE_PERMISSION_CODE = 2222;
    private long lShowWhenMain;
    private final String KEY_TIME_MAIN = "KEY_TIME_MAIN";
    private SwitchCompat swTone;
    private SwitchCompat swNotification;
    private CameraManager mCameraManager;
    private String mCameraId;
    private boolean isFlashAvailable;
    private SwitchCompat swLantern;
    private View btnRingTone;
    private final int NOTIFICATION_ID = 274567891;
    private final int NOTIFICATION_ID_ALARM = 659845965;
    private AudioManager audioManager;
    private AudioManager.OnAudioFocusChangeListener afChangeListener;
    private TextView tvTone;
    private Handler timerHandler;
    private boolean isCharging = false;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        unixTimeStart = currentTimeMillis() / 1000L;
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        tvPercentage = findViewById(R.id.tvPercentage);

        context.registerReceiver(this.broadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        tts = new TextToSpeech(this, this);

        etMinCharge = findViewById(R.id.etnMinCharge);
        etMinCharge.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100")});

        etMaxCharge = findViewById(R.id.etnMaxCharge);
        etMaxCharge.setFilters(new InputFilter[]{new InputFilterMinMax("1", "100")});

        etMinutes = findViewById(R.id.etnMinutes);
        etMinutes.setFilters(new InputFilter[]{new InputFilterMinMax("1", "120")});

        ll_activity_main = findViewById(R.id.ll_activity_main);

        btnRingTone = findViewById(R.id.btnRingTone);

        SharedPreferences sp = getSharedPreferences(myPrefs, MODE_PRIVATE);
        etMinCharge.setText(sp.getString("etMinCharge", "30"));
        etMaxCharge.setText(sp.getString("etMaxCharge", "65"));
        etMinutes.setText(sp.getString("etMinutes", "5"));
        try {
            iMinutes = Integer.parseInt(etMinutes.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            etMinutes.setText("5");
            iMinutes = 5;
        }
        sRingToneUri = sp.getString("sRingToneUri", "NoSound");

        swGoogleVoice = findViewById(R.id.swGoogleVoice);
        swLowVoice = findViewById(R.id.swLowVoice);
        swCheckMinCharging = findViewById(R.id.swCheckMinCharging);
        swCheckMaxNotCharging = findViewById(R.id.swCheckMaxNotCharging);
        swTone = findViewById(R.id.swTone);
        swNotification = findViewById(R.id.swNotification);
        swLantern = findViewById(R.id.swLantern);
        swGoogleVoice.setChecked(sp.getBoolean("swEnableGoogleVoice", false));
        swLowVoice.setChecked(sp.getBoolean("swEnableLowVoice", false));
        swCheckMinCharging.setChecked(sp.getBoolean("swCheckMinCharging", false));
        swCheckMaxNotCharging.setChecked(sp.getBoolean("swCheckMaxNotCharging", false));
        swTone.setChecked(sp.getBoolean("swEnableTone", false));
        swNotification.setChecked(sp.getBoolean("swEnableNotification", false));
        swLantern.setChecked(sp.getBoolean("swEnableLantern", false));

        tvTone = findViewById(R.id.tvTone);
        showTone(Uri.parse(sp.getString("sRingToneUri", getString(R.string.no_sound))));

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(
                Context.TELEPHONY_SERVICE);

        Intent intent = this.getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().containsKey(KEY_TIME_MAIN)
                && ((intent.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) == 0)) {
            lShowWhenMain = intent.getLongExtra(KEY_TIME_MAIN, currentTimeMillis());
        } else {
            lShowWhenMain = currentTimeMillis();
        }

        if (intent != null) {
            intent.putExtra(KEY_TIME_MAIN, lShowWhenMain);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, intent,
                PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel(getString(R.string.notification_channel_BCD_id),
                    getString(R.string.notification_description_main), importance);
            channel.setDescription(getString(R.string.notification_description_main));
            channel.setSound(null, null);
            notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            builder = new Notification.Builder(this, getString(R.string.notification_channel_BCD_id))
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.battery_control_droid_running))
                    .setContentIntent(pendingIntent)
                    .setOnlyAlertOnce(false)
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setShowWhen(true)
                    .setWhen(lShowWhenMain)
                    .setAutoCancel(false);
            notification = builder.build();
            notificationManager.notify(NOTIFICATION_ID, notification);
        } else {
            builderCompat = new NotificationCompat.Builder(this, getString(R.string.notification_id_main))
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round))
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(getString(R.string.battery_control_droid_running))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
                    .setOnlyAlertOnce(false)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setShowWhen(true)
                    .setWhen(lShowWhenMain)
                    .setAutoCancel(false);
            notificationManagerCompat = NotificationManagerCompat.from(this);
            notification = builderCompat.build();
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            notificationManagerCompat.notify(NOTIFICATION_ID, notification);
        }

        isFlashAvailable = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (isFlashAvailable) {
            mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = mCameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        } else {
            swLantern.setVisibility(View.GONE);
        }

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

        etMinCharge.setOnFocusChangeListener((EditText, hasFocus) -> {
            if (!hasFocus) {
                SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
                spe.putString("etMinCharge", etMinCharge.getText().toString());
                spe.apply();
            }
        });

        etMaxCharge.setOnFocusChangeListener((EditText, hasFocus) -> {
            if (!hasFocus) {
                SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
                spe.putString("etMaxCharge", etMaxCharge.getText().toString());
                spe.apply();
            }
        });

        swGoogleVoice.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
            spe.putBoolean("swEnableGoogleVoice", swGoogleVoice.isChecked());
            spe.apply();
            if (isChecked)
                swLowVoice.setChecked(false);
        });

        swLowVoice.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
            spe.putBoolean("swEnableLowVoice", swLowVoice.isChecked());
            spe.apply();
            if (isChecked)
                swGoogleVoice.setChecked(false);
        });

        swTone.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
            spe.putBoolean("swEnableTone", swTone.isChecked());
            spe.apply();
            if (!swTone.isChecked() && myPlayer != null) {
                myPlayer.stop();
                myPlayer.release();
                myPlayer = null;
            }
        });

        swNotification.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
            spe.putBoolean("swEnableNotification", swNotification.isChecked());
            spe.apply();
        });

        swCheckMinCharging.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
            spe.putBoolean("swCheckMinCharging", swCheckMinCharging.isChecked());
            spe.apply();
        });

        swCheckMaxNotCharging.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
            spe.putBoolean("swCheckMaxNotCharging", swCheckMaxNotCharging.isChecked());
            spe.apply();
        });

        swLantern.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isFlashAvailable) {
                SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
                spe.putBoolean("swEnableLantern", swLantern.isChecked());
                spe.apply();
                if (isChecked)
                    switchFlashLight(false);
            }
        });

        etMinutes.setOnFocusChangeListener((EditText, hasFocus) -> {
            if (!hasFocus) {
                try {
                    iMinutes = Integer.parseInt(etMinutes.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    etMinutes.setText("5");
                    iMinutes = 5;
                }
                if (timerHandler != null) {
                    timerHandler.removeCallbacks(timerRunnable);
                    timerHandler.postDelayed(timerRunnable, iMinutes * 60L * 1000L + 10000L);
                }
                SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
                spe.putString("etMinutes", etMinutes.getText().toString());
                spe.apply();
            }
        });

        timerHandler = new Handler();
        timerHandler.postDelayed(timerRunnable, iMinutes * 60L * 1000L);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

            tvPercentage.setText(getString(R.string.battery_charge, level));
            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status ==
                    BatteryManager.BATTERY_STATUS_FULL;
        }
    };

    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     *
     * <p>This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * {@link #onPrepareOptionsMenu}.
     *
     * <p>The default implementation populates the menu with standard system
     * menu items.  These are placed in the {@link Menu#CATEGORY_SYSTEM} group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     *
     * <p>You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     *
     * <p>When you add items to the menu, you can implement the Activity's
     * {@link #onOptionsItemSelected} method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onPrepareOptionsMenu
     * @see #onOptionsItemSelected
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_main_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     *
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.action_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onInit(int status) {
        super.onStart();
        if (status == TextToSpeech.SUCCESS) {
            int result = tts.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            }
        } else {
            Log.e("TTS", "Initialization Failed!");
        }

    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }

        context.unregisterReceiver(broadcastReceiver);

        if (myPlayer != null) {
            if (myPlayer.isPlaying())
                myPlayer.stop();
            myPlayer.release();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.cancel(NOTIFICATION_ID);
            notificationManager.cancel(NOTIFICATION_ID_ALARM);
        } else {
            notificationManagerCompat.cancel(NOTIFICATION_ID);
            notificationManagerCompat.cancel(NOTIFICATION_ID_ALARM);
        }

        if (isFlashAvailable)
            switchFlashLight(false);

        if (timerHandler != null)
            timerHandler.removeCallbacks(timerRunnable);

        super.onDestroy();
    }

    private void setSpeed(String speed) {
        if (speed.equals("Very Slow")) {
            tts.setSpeechRate(0.1f);
        }
        if (speed.equals("Slow")) {
            tts.setSpeechRate(0.5f);
        }
        if (speed.equals("Normal")) {
            tts.setSpeechRate(1.0f);//default 1.0
        }
        if (speed.equals("Fast")) {
            tts.setSpeechRate(1.5f);
        }
        if (speed.equals("Very Fast")) {
            tts.setSpeechRate(2.0f);
        }
        //for setting pitch you may call
        //tts.setPitch(1.0f);//default 1.0
    }

    private void speakOut() {
        tts.speak(getString(R.string.speak_out, level), TextToSpeech.QUEUE_FLUSH, null);
    }

    private void speakOutQuietMode() {
        tts.speak("u   u   u", TextToSpeech.QUEUE_FLUSH, null);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        try {
            if (v == btnRingTone) {
                Intent tmpIntent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                tmpIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALL);
                tmpIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                tmpIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);
                tmpIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));
                Uri currentSound = null;

                String defaultPath = null;
                Uri defaultUri = Settings.System.DEFAULT_RINGTONE_URI;
                if (defaultUri != null) {
                    defaultPath = defaultUri.getPath();
                }
                String path = sRingToneUri;
                if (path != null && !path.equals("NoSound")) {
                    if (path.equals(defaultPath)) {
                        currentSound = defaultUri;
                    } else {
                        currentSound = Uri.parse(path);
                    }
                }
                if (sRingToneUri == null)
                    tmpIntent.putExtra(null, currentSound);
                else
                    tmpIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentSound);
                startActivityForResult(tmpIntent, requestCodeTone);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (data != null && requestCode == requestCodeTone) {
                Uri ringtoneUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

                if (ringtoneUri != null) {
                    sRingToneUri = ringtoneUri.toString();
                } else {
                    sRingToneUri = null;
                }

                SharedPreferences.Editor spe = getSharedPreferences(myPrefs, MODE_PRIVATE).edit();
                spe.putString("sRingToneUri", sRingToneUri);
                spe.apply();
                showTone(ringtoneUri);
            }
        }
    }

    private void sendNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(getString(R.string.notification_channel_BCD_id))
                    .setContentText(getString(R.string.speak_out, level))
                    .setShowWhen(true)
                    .setWhen(currentTimeMillis())
                    .setAutoCancel(true);
            notificationManager.notify(NOTIFICATION_ID_ALARM, builder.build());
        } else {
            builderCompat.setContentText(getString(R.string.speak_out, level));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builderCompat.setShowWhen(true);
            }
            builderCompat.setWhen(currentTimeMillis())
                    .setAutoCancel(true);

            if (ActivityCompat.checkSelfPermission(this,
                    android.Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED)
                notificationManagerCompat.notify(NOTIFICATION_ID_ALARM, builderCompat.build());
        }

    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > currentTimeMillis()) {
            super.onBackPressed();
        } else {
            ll_activity_main.requestFocus();
            Toast.makeText(getBaseContext(), getString(R.string.press_again_exit),
                    Toast.LENGTH_SHORT).show();
        }

        back_pressed = currentTimeMillis();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PHONE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, getString(R.string.read_phone_state_permission_granted),
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, getString(R.string.read_phone_state_permission_deny),
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private void switchFlashLight(boolean status) {
        try {
            mCameraManager.setTorchMode(mCameraId, status);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void showTone(Uri uri) {
        Ringtone ringtone = RingtoneManager.getRingtone(this, uri);
        String title = ringtone.getTitle(this);
        tvTone.setText(title);
    }

    final Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            timerHandler.postDelayed(this, iMinutes * 60L * 1000L + 10000L);

            long unixTimeNow = currentTimeMillis() / 1000L;

            if (unixTimeNow > (unixTimeLastCheckBattery + (iMinutes * 60L)))
                unixTimeLastCheckBattery = unixTimeNow;
            else
                return;

            tvPercentage.setText(getString(R.string.battery_charge, level));

            if ((unixTimeNow - unixTimeStart) > (iMinutes * 60L)) {
                int iMaxBattery, iMinBattery;

                try {
                    iMinBattery = Integer.parseInt(etMinCharge.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    iMinBattery = 30;
                }

                try {
                    iMaxBattery = Integer.parseInt(etMaxCharge.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    iMaxBattery = 65;
                }
                if (((level <= iMinBattery) &&
                        (!isCharging || swCheckMinCharging.isChecked()))
                    || ((level >= iMaxBattery) &&
                        (isCharging || swCheckMaxNotCharging.isChecked()))) {

                    if (swGoogleVoice.isChecked()) {
                        setSpeed("Normal");
                        speakOut();
                    }

                    if (swLowVoice.isChecked()) {
                        setSpeed("Normal");
                        speakOutQuietMode();
                    }

                    if (swTone.isChecked() && (sRingToneUri != null) && !sRingToneUri.equals("NoSound")) {
                        if (myPlayer != null) {
                            myPlayer.release();
                            myPlayer = null;
                        }

                        myPlayer = new MediaPlayer();
                        myPlayer.setAudioAttributes(
                                new AudioAttributes.Builder()
                                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                        .setUsage(AudioAttributes.USAGE_MEDIA)
                                        .build()
                        );
                        try {
                            myPlayer.setDataSource(getApplicationContext(), Uri.parse(sRingToneUri));
                            myPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        myPlayer.start();

                    } else {
                        if (myPlayer != null) {
                            myPlayer.stop();
                            myPlayer.release();
                            myPlayer = null;
                        }
                    }

                    if (swNotification.isChecked())
                        sendNotification();

                    if (isFlashAvailable && swLantern.isChecked())
                        switchFlashLight(true);

                    audioManager.abandonAudioFocus(afChangeListener);
                }


            }
        }
    };
}